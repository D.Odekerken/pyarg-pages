py\_arg.explanation package
===========================

Submodules
----------

py\_arg.explanation.defending module
------------------------------------

.. automodule:: py_arg.explanation.defending
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.explanation.not\_defending module
-----------------------------------------

.. automodule:: py_arg.explanation.not_defending
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.explanation.reach\_and\_dist module
-------------------------------------------

.. automodule:: py_arg.explanation.reach_and_dist
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: py_arg.explanation
   :members:
   :undoc-members:
   :show-inheritance:
