py\_arg.semantics package
=========================

Submodules
----------

py\_arg.semantics.get\_acceptable\_with\_respect\_to module
-----------------------------------------------------------

.. automodule:: py_arg.semantics.get_acceptable_with_respect_to
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.get\_admissible\_sets module
----------------------------------------------

.. automodule:: py_arg.semantics.get_admissible_sets
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.get\_complete\_extensions module
--------------------------------------------------

.. automodule:: py_arg.semantics.get_complete_extensions
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.get\_eager\_extension module
----------------------------------------------

.. automodule:: py_arg.semantics.get_eager_extension
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.get\_grounded\_extension module
-------------------------------------------------

.. automodule:: py_arg.semantics.get_grounded_extension
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.get\_ideal\_extension module
----------------------------------------------

.. automodule:: py_arg.semantics.get_ideal_extension
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.get\_preferred\_extensions module
---------------------------------------------------

.. automodule:: py_arg.semantics.get_preferred_extensions
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.get\_semistable\_extensions module
----------------------------------------------------

.. automodule:: py_arg.semantics.get_semistable_extensions
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.get\_stable\_extensions module
------------------------------------------------

.. automodule:: py_arg.semantics.get_stable_extensions
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.is\_acceptable\_with\_respect\_to module
----------------------------------------------------------

.. automodule:: py_arg.semantics.is_acceptable_with_respect_to
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.is\_admissible module
---------------------------------------

.. automodule:: py_arg.semantics.is_admissible
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.is\_complete module
-------------------------------------

.. automodule:: py_arg.semantics.is_complete
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.is\_conflict\_free module
-------------------------------------------

.. automodule:: py_arg.semantics.is_conflict_free
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.is\_grounded\_extension module
------------------------------------------------

.. automodule:: py_arg.semantics.is_grounded_extension
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.semantics.is\_preferred\_extension module
-------------------------------------------------

.. automodule:: py_arg.semantics.is_preferred_extension
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: py_arg.semantics
   :members:
   :undoc-members:
   :show-inheritance:
