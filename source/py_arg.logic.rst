py\_arg.logic package
=====================

Submodules
----------

py\_arg.logic.closure module
----------------------------

.. automodule:: py_arg.logic.closure
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.logic.is\_c\_consistent module
--------------------------------------

.. automodule:: py_arg.logic.is_c_consistent
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: py_arg.logic
   :members:
   :undoc-members:
   :show-inheritance:
