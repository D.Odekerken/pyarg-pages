py\_arg.dynamic\_aspic\_classes package
=======================================

Submodules
----------

py\_arg.dynamic\_aspic\_classes.axiom\_queryable module
-------------------------------------------------------

.. automodule:: py_arg.dynamic_aspic_classes.axiom_queryable
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.dynamic\_aspic\_classes.ordinary\_queryable module
----------------------------------------------------------

.. automodule:: py_arg.dynamic_aspic_classes.ordinary_queryable
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.dynamic\_aspic\_classes.potential\_argument module
----------------------------------------------------------

.. automodule:: py_arg.dynamic_aspic_classes.potential_argument
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.dynamic\_aspic\_classes.potential\_argumentation\_theory module
-----------------------------------------------------------------------

.. automodule:: py_arg.dynamic_aspic_classes.potential_argumentation_theory
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.dynamic\_aspic\_classes.queryable module
------------------------------------------------

.. automodule:: py_arg.dynamic_aspic_classes.queryable
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: py_arg.dynamic_aspic_classes
   :members:
   :undoc-members:
   :show-inheritance:
