Installation instructions
=========================

1. Clone code from our `GitLab repository <https://git.science.uu.nl/D.Odekerken/py_arg>`_
   into some directory on your computer.

    .. code-block:: bash

        git clone https://git.science.uu.nl/D.Odekerken/py_arg.git

2.  Setup some environment with python and pip. Install the required packages.

    .. code-block:: python

        pip install -r requirements.txt