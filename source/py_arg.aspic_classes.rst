py\_arg.aspic\_classes package
==============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   py_arg.aspic_classes.orderings

Submodules
----------

py\_arg.aspic\_classes.argumentation\_system module
---------------------------------------------------

.. automodule:: py_arg.aspic_classes.argumentation_system
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.argumentation\_theory module
---------------------------------------------------

.. automodule:: py_arg.aspic_classes.argumentation_theory
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.axiom module
-----------------------------------

.. automodule:: py_arg.aspic_classes.axiom
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.defeasible\_rule module
----------------------------------------------

.. automodule:: py_arg.aspic_classes.defeasible_rule
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.instantiated\_argument module
----------------------------------------------------

.. automodule:: py_arg.aspic_classes.instantiated_argument
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.literal module
-------------------------------------

.. automodule:: py_arg.aspic_classes.literal
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.ordinary\_premise module
-----------------------------------------------

.. automodule:: py_arg.aspic_classes.ordinary_premise
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.preference module
----------------------------------------

.. automodule:: py_arg.aspic_classes.preference
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.rule module
----------------------------------

.. automodule:: py_arg.aspic_classes.rule
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.strict\_rule module
------------------------------------------

.. automodule:: py_arg.aspic_classes.strict_rule
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: py_arg.aspic_classes
   :members:
   :undoc-members:
   :show-inheritance:
