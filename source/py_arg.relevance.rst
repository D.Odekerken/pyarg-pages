py\_arg.relevance package
=========================

Submodules
----------

py\_arg.relevance.get\_relevant\_literals\_for\_potential\_argument module
--------------------------------------------------------------------------

.. automodule:: py_arg.relevance.get_relevant_literals_for_potential_argument
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.relevance.get\_relevant\_potential\_arguments\_for\_adding\_to\_NG module
---------------------------------------------------------------------------------

.. automodule:: py_arg.relevance.get_relevant_potential_arguments_for_adding_to_NG
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.relevance.get\_relevant\_potential\_arguments\_for\_removing\_from\_PG module
-------------------------------------------------------------------------------------

.. automodule:: py_arg.relevance.get_relevant_potential_arguments_for_removing_from_PG
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: py_arg.relevance
   :members:
   :undoc-members:
   :show-inheritance:
