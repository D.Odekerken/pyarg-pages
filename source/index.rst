PyArg Documentation site
========================
PyArg is a python-based solver and explainer for both abstract argumentation and ASPIC+.
A large variety of extension-based semantics allows for flexible evaluation.
In addition, several explanation functions are available.

This documentation website lists the modules and functions of PyArg.
An interactive visualisation of selected functionality of PyArg can be found `here <https://pyarg.herokuapp.com/>`_.
For the source code, we refer to `our GitLab repository <https://git.science.uu.nl/D.Odekerken/py_arg>`_.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   installation
   example_usage
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
