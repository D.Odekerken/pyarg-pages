py\_arg.aspic\_classes.orderings package
========================================

Submodules
----------

py\_arg.aspic\_classes.orderings.democratic\_ordering module
------------------------------------------------------------

.. automodule:: py_arg.aspic_classes.orderings.democratic_ordering
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.orderings.elitist\_ordering module
---------------------------------------------------------

.. automodule:: py_arg.aspic_classes.orderings.elitist_ordering
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.orderings.last\_link\_ordering module
------------------------------------------------------------

.. automodule:: py_arg.aspic_classes.orderings.last_link_ordering
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.orderings.ordering module
------------------------------------------------

.. automodule:: py_arg.aspic_classes.orderings.ordering
   :members:
   :undoc-members:
   :show-inheritance:

py\_arg.aspic\_classes.orderings.weakest\_link\_ordering module
---------------------------------------------------------------

.. automodule:: py_arg.aspic_classes.orderings.weakest_link_ordering
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: py_arg.aspic_classes.orderings
   :members:
   :undoc-members:
   :show-inheritance:
